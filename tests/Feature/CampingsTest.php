<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\User;
use App\Models\Camping;

class CampingsTest extends TestCase
{

    // use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /** @test */
    public function only_logged_in_users_can_see_all_campings_list()
    {
        
        $response = $this->get('/campings/admin')->assertRedirect('/login');
    }

    /** @test */
    public function authenticaded_users_can_see_the_campings_list()
    {
        
        User::factory()->create();
        $response = $this->get('/')->assertOk();

    }

    /** @test */
    public function create_new_camping()
    {
        
        Camping::factory()->count(1)->create([
            'camping_name' => 'campingsTest'
        ]);

        $response = $this->assertDatabaseHas('campings', ['camping_name' => 'campingsTest']);    

    }   



}

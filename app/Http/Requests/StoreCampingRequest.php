<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCampingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
        // return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'camping_name'=> ['required', 'min:8', 'max:20'],
            'country' => ['required', 'min:5', 'max:15'],
            'city' => ['required', 'min:5', 'max:15'],
            'website' => ['required', 'regex:/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i'],
            'rating' => ['required', 'numeric|min:1|max:5'],
            'main_photo' => ['required', 'image', 'nullable','max:1999'],
            'tags' => 'exists:tags,id' // check whet
        ];

    }
}

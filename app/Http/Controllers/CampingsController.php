<?php

namespace App\Http\Controllers;

use App\Models\Camping;
use App\Models\Tag;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Bus;
use App\Http\Requests\StoreCampingRequest;
use App\Http\Resources\CampingsResource;
use App\Services\CampingService;


// use App\Jobs\CampingsCsvImport;


class CampingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
  
        if (request('tag')){
            //if in url request exist "tag" load campings only wit associated tag
            $campings = Tag::where('name',request('tag'))->firstOrFail()->campings()->paginate(10);           
            
        }   else {
            $campings = Camping::latest()->paginate(10);
             
        }

        $rated_campings = Camping::with('tags')->orderBy('rating', 'asc')->paginate(10);

        return view('welcome', ['campings' => $campings] , ['rated_campings' => $rated_campings]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
        return view('campings.create', [
            'tags' => Tag::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        $this->validateCamping();

        $camping = new Camping(request(['camping_name', 'main_photo', 'rating', 'country', 'city', 'website' ]));

        $camping->user_id = auth()->user()->id;
        $camping->main_photo = $this->saveCampingPhoto($request);

        $camping->save();

        $camping->tags()->attach(request('tags'));
        

        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Camping  $camping
     * @return \Illuminate\Http\Response
     */
    public function show(Camping $camping)
    {
        return view('campings.show', ['camping' => $camping]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Camping  $camping
     * @return \Illuminate\Http\Response
     */
    public function edit(Camping $camping)
    {

         return view('campings.edit', ['camping'=>$camping, 'tags' => Tag::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Camping  $camping
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Camping $camping)
    {

        $this->validateCamping();

        $camping->main_photo = $this->saveCampingPhoto($request);
        $camping->country = request('country');
        $camping->camping_name = request('camping_name');
        $camping->city = request('city');
        $camping->rating = request('rating');

        $camping->save();

        $camping->tags()->sync(request('tags'));
        
        return redirect(route('campings.show', $camping));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Camping  $camping
     * @return \Illuminate\Http\Response
     */
    public function destroy(Camping $camping)
    {
        //
        $camping->delete();
        return redirect(route('campings.admin', $camping));
    }

    public function admin()
    {

            $campings = Camping::latest()->get();

        return view('campings.admin', ['campings' => $campings]);
    }

    public function importCampingsFromCsv(Request $request) {
        

        if($request->hasFile('filecsv')) { 
        
            CampingService::importFile($request);
        }

        return redirect('/campings/create');

    }

    public function exportcsv()
    {   
        
        $name = 'campings.csv';
        $headers = ['Content-Disposition' => 'attachment; filename='. $name,];
        $columns = \Illuminate\Support\Facades\Schema::getColumnListing("campings");
        array_Shift($columns);
        $data = Camping::all();

        $callback = function() use($data, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($data as $key => $value) {
                $data = $value->toArray();
                
                unset($data['id']);

                fputcsv($file, $data);
            }

            fclose($file);

        };

        return response()->stream($callback, 200,  $headers );
         
    }

  
    protected function validateCamping()
    {
        return request()->validate([
            'camping_name'=> ['required', 'min:8', 'max:20'],
            'country' => ['required', 'min:5', 'max:15'],
            'city' => ['required', 'min:5', 'max:15'],
            'website' => ['required', 'regex:/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i'],
            'rating' => ['required', 'numeric', 'min:1', 'max:5'],
            'main_photo' => ['required', 'image', 'nullable','max:1999'],
            'tags' => ['exists:tags,id'] // check whether tag exist in DB.  we look in id column
           
        ]);
    }

    protected function tags() {
        return (['tags' => Tag::all()]);
    }

    protected function batch() {
        $batchId = request('id');
        dd(bus::findBatch($batchId));
    }

    public function importProgress()
    {
        
        $batches = DB::table('job_batches')->where('pending_jobs', '>', 0)->get();
        
        if (count($batches) > 0) {
            return Bus::findBatch($batches[0]->id);
        }

        return [];
    }

    public function saveCampingPhoto($request) {
        
        // handle dfile upload
        if($request->hasFile('main_photo')) {

            // get file name with extension
            $fileNameWithExt = $request->file('main_photo')->getClientOriginalName();

            // get just file name
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            
            // get just ext
            $extension = $request->file('main_photo')->getClientOriginalExtension();

            // file name to store

            $fileNameToStore = $fileName.'_'.time().'.'.$extension;

            //upload image

            $path = $request->file('main_photo')->storeAs('/main_photo', $fileNameToStore);
            
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        return $fileNameToStore;
    }

}

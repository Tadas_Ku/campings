<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Bus\Batchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Models\Camping;

class CampingsCsvImport implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $parsedCsv;
    public $header;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($parsedCsv,$header)
    {

        $this->parsedCsv = $parsedCsv;
        $this->header = $header;

        // dd($x);
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
  
        foreach ($this->parsedCsv as $value) {

            $campingData = array_combine($this->header, $value);
            Camping::create($campingData);
            
        }

     }

     public function failed(Throwable $exception)
     {
         
     }

}

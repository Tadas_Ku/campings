<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Camping extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function getCampingPhoto($value)
    {
        return asset($value);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}

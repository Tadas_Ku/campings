<?php

namespace App\Services;
use Illuminate\Support\Facades\DB;
use App\Models\Camping;
use App\Models\Tag;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Bus;
use App\Http\Requests\StoreCampingRequest;
use App\Http\Resources\CampingsResource;
use App\Jobs\CampingsCsvImport;

class CampingService {


   public static function importFile($request) {

        $parsedCsv = file(request()->filecsv);

        // Split an array into chunks, array_chunk Returns a multidimensional numerically indexed array
        $SplidetCsvFiles = array_chunk($parsedCsv, 1);

        // Write records from SplidetCsvFile array to .csv file
        $header = [];

        // The dispatch function pushes the given job onto the Laravel job queue:
        $batch = Bus::batch([])->dispatch();


        foreach ($SplidetCsvFiles as $key => $SplidetCsvFile) {

            // str_getcsv — Parse a CSV string into an array
            // one liner to parse a CSV file into an array -> array_map('str_getcsv', file('data.csv'));
            $parsedCsv = array_map('str_getcsv', $SplidetCsvFile);
          
            if ($key === 0) {
                $header = $parsedCsv[0];  // $header = $parsedCsv[0];  // header = column names
                unset($parsedCsv[0]);  // unset($parsedCsv[0]); // unset headers from array
            }

            $batch->add(new CampingsCsvImport($parsedCsv,$header));
 
        }
        
        return;
                
    }

}
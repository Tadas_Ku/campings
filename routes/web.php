<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CampingsController;
use App\Models\Camping;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\CampingsController@index')->name('campings.index');

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Route::get('/campings', 'App\Http\Controllers\CampingsController@index')->name('campings.index');
Route::group(['middleware' => 'auth'], function() {
    Route::get('/campings/admin', 'App\Http\Controllers\CampingsController@admin')->name('campings.admin');
    Route::get('/campings/create', 'App\Http\Controllers\CampingsController@create')->name('campings.create');
    Route::POST('/campings', 'App\Http\Controllers\CampingsController@store');
    Route::get('/campings/{camping}/edit', 'App\Http\Controllers\CampingsController@edit')->name('campings.edit');
    Route::PUT('/campings/{camping}', 'App\Http\Controllers\CampingsController@update');


    Route::get('/campings/{camping}/destroy', 'App\Http\Controllers\CampingsController@destroy')->name('campings.destroy'); 
    // Route::get('/campings-csvexport', 'App\Http\Controllers\CampingsController@exportcsv')->name('exportcsv.csv');
    // Route::get('/campings-csvimport', 'App\Http\Controllers\CampingsController@importcsv')->name('importcsv.csv');
    Route::get('/campings-tags', 'App\Http\Controllers\CampingsController@tags')->name('tags');
    Route::post('/campings-getdocument', 'App\Http\Controllers\CampingsController@importCampingsFromCsv')->name('getdocument.csv');

    Route::get('/campings-csvexport', 'App\Http\Controllers\CampingsController@exportcsv')->name('exportcsv.csv');
});



Route::get('/campings', [CampingsController::class, 'index'])->name('campings.index');

Route::get('/campings/{camping}', 'App\Http\Controllers\CampingsController@show')->name('campings.show');

Route::get('/batch', [CampingsController::class, 'batch']);


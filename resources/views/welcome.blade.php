@extends ('layouts\app')

@section('content')

    <div class="grid-container">
        <div class="gridleft">
            
            @forelse ($campings as $camping)
                <div class="single_camping" style="">
                    
                    {{-- check how the image should be displayed --}}
                    @if (str_contains($camping->main_photo, "jpg"))
                        <img  src="{{asset('/storage/main_photo/'.$camping->main_photo) }}" alt="test">
                    @else
                        <img src="{{'https://source.unsplash.com/featured/?travel,'.$camping->main_photo}}" alt="test">
                    @endif
                    
                    <div class="single_camping_info">
                        <h5>{{ $camping->camping_name}}</h5>
                        <div class="location_info">
                            {{$camping->country}} &#9900 {{$camping->city}} 

                            @for($i = 0; $i < $camping->rating; $i++)
                            <span> &#10030 </span>
                            @endfor

                        </div>

                        <div>

                            <div class="review">
                               <span> Very good 9.2 / 10 </span> 
                            </div>

                            <div class="information-button">
                                <a class="btn fs12"  href="{{route('campings.show', $camping)}}" >More information</a>
                            </div>
 
                            <div class="website_link">
                                <span>  Where to book? </span>
                            </div>    

                        </div>

                    </div>  {{-- <div class="single_camping_info"> --}}
                       
                </div>       
             
            @empty
            <p>no articles</p>
            @endforelse
              

                
        </div>  {{-- <div class="gridleft"> --}}
            

        <div class="gridright">
            @forelse ($rated_campings as $rated_camping)
            <div style="padding: 10px; display: flex; ">
                <div>

                    {{-- check how the image should be displayed --}}
                @if (str_contains($camping->main_photo, "jpg"))
                     <img src="{{asset('/storage/main_photo/'.$rated_camping->main_photo) }}" alt="test">
                @else
                    <img src="{{'https://source.unsplash.com/featured/?travel,'.$rated_camping->main_photo}}" alt="test">
                @endif

                </div>
                <div class="rated_camping_info">
                    <a   href="{{route('campings.show', $rated_camping)}}" >
                        <h4>{{ $rated_camping->camping_name}}</h4>
                    </a>
                    
                    <div class="location_info">
                        {{$rated_camping->country}} &#9900 {{$rated_camping->city}} 
                        @for($i = 0; $i < $rated_camping->rating; $i++)
                             <span> &#10030 </span>
                        @endfor
                    </div>

                    <div>
                        <div >
                           <span> Very good 9.2 / 10 </span> 
                        </div>
                        <div class="camping_tags">
                                @foreach ($rated_camping->tags->slice(0, 3) as $tag)
                                    <a href="/?tag={{ $tag->name }}">{{ $tag->name }}</a>
                                @endforeach
                        </div>

                    </div>

                </div> 

            </div>
                
            @empty
                
            @endforelse
        </div>
        
    </div> {{-- class="grid-container" --}}
    {{ $campings->links() }} 
@endsection


@extends ('layouts\app')

@section('content')

    <div class="grid-container">
        <div class="gridleft">
            @forelse ($campings as $camping)
                <div class="single_camping" style="">
            {{-- <img src="{{'https://picsum.photos/200/300?random='.$camping->main_photo}}" alt="test" style="width:100%; height:200px; border-radius: 13px 13px 0px 0px;"> --}}
            <img src="{{'https://source.unsplash.com/featured/?travel,'.$camping->main_photo}}" alt="test" style="width:100%; height:250px; border-radius: 13px 13px 0px 0px; object-fit: cover;">
           
            {{-- <img src="https://source.unsplash.com/random/200x200?sig=1" /> --}}
                    {{-- <img src="{{asset('/storage/public/main_photo/'.$camping->main_photo) }}" alt="test" style="width:100%; height:200px; border-radius: 13px 13px 0px 0px;"> --}}
                    <div class="single_camping_info">
                        <h5>{{ $camping->camping_name}}</h5>
                        <div style="color: #fc4c02">
                            {{$camping->country}} * {{$camping->city}} 

                            @for($i = 0; $i < $camping->rating; $i++)
                            <span> &#9734 </span>
                            @endfor

                        </div>

                        <div>
                            <div >
                               Rating {{ $camping->rating }}
                            </div>
                            <div class="information-button" data-v-39652765="" data-v-03472436="">
                                <a class="btn fs12" target="_blank" href="{{route('campings.show', $camping)}}" >More information</a>
                            </div>
                            {{-- <button class="more_info_btn">
                                <a id="test" href="{{route('campings.show', $camping)}}">
                                    More information
                                </a> 
                            </button> --}}
                        <p class="website_link">
                            Where to book?
                        </div>

                    </div>  {{-- <div class="single_camping_info"> --}}
                </div>       

            @empty
            <p>no articles</p>
            @endforelse
            

           
        </div>  {{-- <div class="gridleft"> --}}


        <div class="gridright">
            @forelse ($rated_campings as $rated_camping)
            <div style="padding: 10px; display: flex; ">
                <div>
                {{-- <img src="{{asset('/storage/public/main_photo/'.$rated_camping->main_photo) }}" alt="test" style="width:200px; height:150px;"> --}}
                <img src="{{'https://source.unsplash.com/featured/?travel,'.$rated_camping->main_photo}}" alt="test" style="width:200px; height:150px; border-radius: 13px 13px 0px 0px; object-fit: cover;">

                </div>
                <div class="rated_camping_info" style="padding: 10px;"
                     >
                    <h4>{{ $rated_camping->camping_name}}</h4>
                    <div>
                    {{$rated_camping->country}} * {{$rated_camping->city}}  &#9734 &#9734 &#9734 
                    </div>

                    <div>
                        <div >
                           Rating {{ $rated_camping->rating }}
                        </div>
                        <div class="camping_tags">
                            <p>
                                @foreach ($rated_camping->tags as $tag)
                                    <a href="/campings?tag={{ $tag->name }}">{{ $tag->name }}</a>
                                @endforeach
                            </p>
                        </div>
                    </div>

                </div> 

            </div>
                
            @empty
                
            @endforelse
        </div>
        
    </div> {{-- class="grid-container" --}}

    <div>
        <form class="form-horizontal" action="/campings" method="post" name="upload_excel"   
                  enctype="multipart/form-data">
              <div class="form-group">
                        <div class="col-md-4 col-md-offset-4">
                            <input type="submit" name="Export" class="btn btn-success" value="export to excel"/>
                        </div>
               </div>                    
        </form>           
    </div>

@endsection


@extends ('layouts\app')

@section('content')
 
    <div class="grid-container">
            <table>
                <tr>
                    <th>Camping name</th>
                    <th>Country / City</th>
                    <th>Rating</th>
                    <th>Edit</th>
                    <th>Delete</th>
                  </tr>
                @foreach ($campings as $camping)
                <tr>
                    <td><a href="{{ route('campings.show', $camping) }}"> {{ $camping->camping_name}}</a></td>
                    <td>{{ $camping->country}} / {{ $camping->city}} </td>
                    <td>{{ $camping->rating}}</td>
                    <td><a href="{{ route('campings.edit', $camping) }}"> Edit</a></td>
                    <td><a href="{{ route('campings.destroy', $camping) }}"> X </a></td>
                </tr>
                @endforeach

            </table>

        <div class="gridright">
        </div>
        
    </div> {{-- class="grid-container" --}}



@endsection


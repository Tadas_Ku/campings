@extends ('layouts\app')

@section('content')

<div class="form_create_edit">
    <div class="form-style-2">
        <div class="form-style-2-heading">Import / Export data</div>
        <a href="{{ route('exportcsv.csv') }}">EXPORT all campings</a><br><br>
        
        <form method="POST" action="{{ route('getdocument.csv') }}" enctype="multipart/form-data">
                 @csrf
            <label for="filecsv">
                <span>Choose .CSV file to import</span>
                <input
                required
                class="input-field" 
                style="@error('filecsv') border: solid red; @enderror" 
                type="file" 
                name="filecsv" 
                id="main_photo"
                value="{{old('filecsv')}}">
    
                @error('filecsv')
            <p class="" style="color:red">{{ $errors->first('filecsv') }}</p>
                
            @enderror
        </label>
        <label><span> </span><input type="submit" value="Submit" /></label>
        </form>
        </div>
    
    
    
    
    
    <div class="form-style-2">
        <div class="form-style-2-heading">Camping information</div>
        <form method="Post" action="/campings/{{ $camping->id }}" enctype="multipart/form-data">
            @csrf
            @method('PUT')
        <label for="camping_name">
            <span>Camping name  <span class="required">*</span></span>
            <input
            required
            class="input-field" 
            style="@error('camping_name') border: solid red; @enderror" 
            type="text" 
            name="camping_name" 
            id="camping_name"
            value="{{ $camping->camping_name}}">
            
            @error('camping_name')
            <p class="" style="color:red">{{ $errors->first('camping_name') }}</p>
                
            @enderror
        </label>
    
        <label for="country">
            <span>Country  <span class="required">*</span></span>
            <input
            required
            class="input-field" 
            style="@error('country') border: solid red; @enderror" 
            type="text" 
            name="country" 
            id="country"
            value="{{ $camping->country}}">
    
            @error('country')
            <p class="" style="color:red">{{ $errors->first('country') }}</p>
                
            @enderror
        </label>
    
        <label for="city">
            <span>City  <span class="required">*</span></span>
            <input
            required
            class="input-field" 
            style="@error('city') border: solid red; @enderror" 
            type="text" 
            name="city" 
            id="city"
            value="{{ $camping->city }}">
    
            @error('city')
            <p class="" style="color:red">{{ $errors->first('city') }}</p>
                
            @enderror
        </label>
    
        <label for="rating">
            <span>Rating  <span class="required">*</span></span>
            <input
            required
            class="input-field" 
            style="@error('rating') border: solid red; @enderror" 
            type="number" 
            name="rating" 
            id="rating"
            value="{{ $camping->rating }}"
            max="5"
            min="1">
    
            @error('rating')
            <p class="" style="color:red">{{ $errors->first('rating') }}</p>
                
            @enderror
        </label>
    
        <label for="website">
            <span>Website  <span class="required">*</span></span>
            <input
            required
            class="input-field" 
            style="@error('website') border: solid red; @enderror" 
            type="text" 
            name="website" 
            id="website"
            value="{{ $camping->website }}">
    
            @error('website')
            <p class="" style="color:red">{{ $errors->first('website') }}</p>
                
            @enderror
        </label>
    
        <label for="tags"><span>Tags</span>
            <select name="tags[]" multiple class="select-field">
                @foreach ($tags as $tag)
                    <option value="{{ $tag->id }}"
                        @foreach ($camping->tags as $selected)
                            @if ($tag->id === $selected->id)
                                {{ "selected" }}
                            @endif
                        @endforeach
                        >{{ $tag->name }}</option>
                @endforeach
            </select>
            @error('tags')
            <p class="" style="color:red">{{ $errors->first('tags') }}</p>
                
            @enderror
        </label>
    
        <label for="main_photo">
            <span>Photo  <span class="required">*</span></span>
            <input
            class="input-field" 
            style="@error('main_photo') border: solid red; @enderror" 
            type="file" 
            name="main_photo" 
            id="main_photo"
            value="{{$camping->main_photo}}">
    
            @error('main_photo')
            <p class="" style="color:red">{{ $errors->first('main_photo') }}</p>
                
            @enderror
        </label>
    
        
        <div class="field">
            <div class="control">
                <button class="button is-link" type="submit">Submit</button>
            </div>
        </div>
        
        </form>
        </div>
</div>

@endsection
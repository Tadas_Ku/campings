@extends ('layouts\app')

@section('content')
    <div class="show-camping">
        <div class="show-camping-info">
            <div class="camping-header-block">
                <div class="title">
                    <h4>{{ $camping->camping_name}}</h4>
                </div>
                @if (Auth::check())
                    <a class="admin-button" href="{{ route('campings.edit', $camping) }}"> Edit</a>
                    <a class="admin-button" href="{{ route('campings.destroy', $camping) }}"> Delete </a>
                @endif
                <div class="camping-header-block__location">
                    <span style='font-size:20px; padding-right:10px;'>&#x279C;</span>{{$camping->country}} , {{$camping->city}}
                </div>
                <div class="camping_tags">
                        @foreach ($camping->tags as $tag)
                            <a  class="camping_tags" href="/campings?tag={{ $tag->name }}">{{ $tag->name }}</a>
                        @endforeach
                </div>
            </div>
            <div class="accommodation-main-photo">
                @if (str_contains($camping->main_photo, "jpg"))
                    <img src="{{asset('/storage/main_photo/'.$camping->main_photo) }}" alt="">
                @else
                    <img src="{{'https://source.unsplash.com/featured/?travel,'.$camping->main_photo}}" alt="">
                @endif
    
            </div>
            <div class="accommodation-photos">
            <img src="{{'https://source.unsplash.com/featured/?travel,1'}}" alt="">
            <img src="{{'https://source.unsplash.com/featured/?travel,2'}}" alt="">
            <img src="{{'https://source.unsplash.com/featured/?travel,3'}}" alt="">
            <img src="{{'https://source.unsplash.com/featured/?travel,4'}}" alt="">
             </div>
            <div class="intro-text-block">
                Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type specimen book. It usually begins with:
                <br>
                “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.”
                The purpose of lorem ipsum is to create a natural looking block of text (sentence, paragraph, page, etc.) that doesn't distract from the layout. A practice not without controversy, laying out pages with meaningless filler text can be very useful when the focus is meant to be on design, not content.
                <br>
                The passage experienced a surge in popularity during the 1960s when Letraset used it on their dry-transfer sheets, and again during the 90s as desktop publishers bundled the text with their software. Today it's seen all around the web; on templates, websites, and stock designs. Use our generator to get your own, or read on for the authoritative history of lorem ipsum.
            </div>
            
            </div>
            <div>
        </div>
    </div>

  @endsection
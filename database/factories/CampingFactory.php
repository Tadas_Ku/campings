<?php

namespace Database\Factories;

use App\Models\Camping;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;

class CampingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Camping::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */

    // public static function image($dir = null, $width = 640, $height = 480, $category = null, $fullPath = true, $randomize = true, $word = null) 
    public function definition()
    {
        return [
            'user_id' => User::all()->random()->id,
            'camping_name' => $this->faker->Company,
            'country' => $this->faker->Country,
            'city' => $this->faker->City,
            'website' => $this->faker->url,
            'rating' => rand(1,5),
            'main_photo' => $this->faker->randomDigit
        ];

    }

    
}

